#!/bin/bash
# quick & dirty plot
# uses pyxplot/gnuplot to create a quick & dirty xy plot from a single-file

# defcolors=(blue olivegreen brickred grey sepia tan black magenta periwinkle orange)
xcol=1
ycol=()
key="above"
width=12
dpi=127

plottingtool="pyxplot"
pdf=-1
png=-1

# defaults
pyxplot_defaultcolor=(blue black green brickred gray turquoise magenta pinegreen sepia burntorange)   
gnuplot_defaultcolor=(blue black green brown gray turquoise magenta forest-green red orange)
defaultpt=(1 15 2 3 4 5 6 7 8 9)

usage() {
 cat << EOF 1>&2
usage: $0 [options] [-o output] [datafile]
 no datafile indicates stdin
 if no output is given, basename is assumed equal to datafile
 if neither output nor datafile given, basename output is qdp
 either .png and .pdf will be appended to output
     --plottingtool <string>    pyxplot (default) or gnuplot
     --ycol "<ints>"            set y columns in datafile, default all
     --xcol <int>               set x column in datafile, default ${xcol}
     --xlabel <string>          set x axis label, default none
     --ylabel <string>          set y axis label, default none
     --xrange <range>           set x axis range, default auto
     --yrange <range>           set y axis range, default auto
     --grid                     set grid, default no
     --width <float>            set plot width in cm, default ${width}
     --with "<strings>"         set plotting styles, default linespoints
     --lt "<strings>"           set linetypes, default 1
     --lw "<floats>"            set linewdiths, default 1
     --plw "<floats>"           set pointlinewidths, default 1
     --ps "<float>"             set pointsizes, default 1
     --pt "<ints>"              set pointtype, default some values
     --pi "<ints>"              set pointinterval (extension to pyxplot)
     --color "<strings>"        set colors, default blue, black, green, etc.
     --ti "<strings>"           set column titles, default none
     --plottitle <string>       set plot title, default none
     --key <string>             set key location, default above
     --pre <string>             execute pyxplot commands in  <string> before plotting
     --pdf                      generate only pdf output, default both
     --png                      generate only png output, default both
     --dpi <float>              dpi to use in png output, default ${dpi}
     --header <file>            use <file> as template instead of default
     --debug                    leave the .ppl/.plt file for inspection and edition     

 quotes are needed if several columns are to be plotted
 see the README.md file and the examples subdirectory
EOF
  exit 1
}

ARGS=`getopt -o ":o:h" -l "\
plottingtool:,\
plottitle:,\
ti:,\
xcol:,\
ycol:,\
xlabel:,\
ylabel:,\
xrange:,\
yrange:,\
grid,\
width:,\
with:,\
lt:,\
lw:,\
plw:,\
ps:,\
pt:,\
pi:,\
color:,\
pre:,\
header:,\
key:,\
pdf,\
png,\
dpi:,\
debug,\
help" -n "$0" -- "$@"`

# echo $ARGS
if [ $? -ne 0 ];
then
  exit 1
fi
eval set -- "$ARGS"
 
# Now go through all the options
while true;
do
  case "$1" in
    -h|--help)
      usage
      exit 1;;
      
    --plottingtool)
      if [ -n "$2" ];
      then
        plottingtool="$2"
      fi
      shift 2;;

    --plottitle)
      if [ -n "$2" ];
      then
        plottitle="$2"
      fi
      shift 2;;

    --ti)
      if [ -n "$2" ];
      then
        ti=($2)
      fi
      shift 2;;

    -o)
      if [ -n "$2" ];
      then
        output="$2"
      fi
      shift 2;;

    --xcol)
      if [ -n "$2" ];
      then
        xcol="$2"
      fi
      shift 2;;

    --ycol)
      if [ -n "$2" ];
      then
        ycol=($2)
      fi
      shift 2;;

    --xlabel)
      if [ -n "$2" ];
      then
        xlabel="$2"
      fi
      shift 2;;

    --ylabel)
      if [ -n "$2" ];
      then
        ylabel="$2"
      fi
      shift 2;;

    --xrange)
      if [ -n "$2" ];
      then
        xrange="$2"
      fi
      shift 2;;

    --yrange)
      if [ -n "$2" ];
      then
        yrange="$2"
      fi
      shift 2;;

    --width)
      if [ -n "$2" ];
      then
        width=$2
      fi
      shift 2;;

    --with)
      if [ -n "$2" ];
      then
        with=($2)
      fi
      shift 2;;

    --grid)
      grid=1
      shift 1;;

    --lt)
      if [ -n "$2" ];
      then
        lt=($2)
      fi
      shift 2;;

    --lw)
      if [ -n "$2" ];
      then
        lw=($2)
      fi
      shift 2;;

    --plw)
      if [ -n "$2" ];
      then
        plw=($2)
      fi
      shift 2;;

    --ps)
      if [ -n "$2" ];
      then
        ps=($2)
      fi
      shift 2;;

    --pt)
      if [ -n "$2" ];
      then
        pt=($2)
      fi
      shift 2;;

    --pi)
      if [ -n "$2" ];
      then
        pi=($2)
      fi
      shift 2;;

    --color)
      if [ -n "$2" ];
      then
        color=($2)
      fi
      shift 2;;

    --pre)
      if [ -n "$2" ];
      then
        pre="$2"
      fi
      shift 2;;

    --header)
      if [ -n "$2" ];
      then
        header="$2"
      fi
      shift 2;;
 
    --key)
      if [ -n "$2" ];
      then
        key="$2"
      fi
      shift 2;;

    --pdf)
      pdf=1
      png=0
      shift 1;;

    --png)
      png=1
      pdf=0
      shift 1;;

    --dpi)
      if [ -n "$2" ];
      then
        dpi="$2"
      fi
      shift 2;;

    --debug)
      debug=1
      shift 1;;

    --)
      shift
      break;;
  esac
done

shift $((OPTIND-1))

if [ -z "$1" ]; then
   data=`mktemp`
   cat > $data
else
  data="$1"
fi

if [ ! -f "$data" ]; then
 echo "error: cannot open ${data}"
 exit 1;
fi

if [ -z "$output" ]; then
  if [ -z "$1" ]; then
    output="qdp"
  else
    output="$1"
  fi
fi

if [ ! -z "$debug" ]; then
  if [ "x$plottingtool" == "xpyxplot" ]; then
    plotfile="${output}.ppl"
  else
    plotfile="${output}.plt"
  fi
  if [ -e ${plotfile} ]; then
    echo "error: the debug option was given and ${plotfile} already exists"
    exit 1
  fi
else
  plotfile=`mktemp`
fi

cat << EOF > ${plotfile}
# automatically generated by qdp on `date`
#
EOF

if [ ! -z "${header}" ]; then
 if [ ! -e "${header}" ]; then
  echo "header file ${header} does not exist"
  exit 1
 fi
 cat ${header} >> ${plotfile}
else
 if [ "x$plottingtool" == "xpyxplot" ]; then
 # default header for pyxplot
  cat << EOF >> ${plotfile}
set preamble "\\usepackage{amsmath}"
set width ${width}*unit(cm)
set axis x arrow nomirrored
set axis y arrow nomirrored
#set nomxtics

set style data linespoints
EOF
 else
  cat << EOF >> ${plotfile}

set xtics border nomirror in
set ytics border nomirror in

set style data linespoints
EOF
 fi
fi

if [ ! -z "${pre}" ]; then
  echo ${pre} >> ${plotfile}
fi

echo >> ${plotfile}

# # read information from the data file itself
# for field in title xlabel ylabel xrange yrange key; do
#  if [ -z "`eval echo \\$${field}`" ]; then
#    eval ${field}="`grep \"# ${field}\" ${data} | cut -d \" \" -f3-`"
#  fi
# done
title=`grep "# plottitle" ${data} | cut -d' ' -f3-`
if [ -z "${xlabel}" ]; then
  xlabel=`grep "# xlabel" ${data} | cut -d' ' -f3-`
fi
if [ -z "${ylabel}" ]; then
  ylabel=`grep "# ylabel" ${data} | cut -d' ' -f3-`
fi
if [ -z "${xrange}" ]; then
  xrange=`grep "# xrange" ${data} | cut -d' ' -f3-`
fi
if [ -z "${yrange}" ]; then
  yrange=`grep "# yrange" ${data} | cut -d' ' -f3-`
fi

# quoted fields
for field in title xlabel ylabel; do
 #  if not empty, write it into the plot file
 if [ ! -z "`eval echo \\$${field}`" ]; then
  eval echo "set ${field} \\\"\$${field}\\\"" >> ${plotfile}
 fi
done

# unquoted
for field in xrange yrange key; do
 #  if not empty, write it into the plot file
 if [ ! -z "`eval echo \\$${field}`" ]; then
  eval echo "set ${field} \$${field}" >> ${plotfile}
 fi
done


if [ ! -z "$grid" ]; then
 echo "set grid" >> ${plotfile}
fi

# we translate points to dashes because pdflatex does not like points in filenames
escappedoutput=`echo $output | sed s/\\\\./-/g`
if [ ${pdf} -ne 0 ]; then
 if [ "x$plottingtool" == "xpyxplot" ]; then
  cat << EOF >> ${plotfile}
set terminal pdf
set output "${escappedoutput}.pdf"
EOF
 else
  cat << EOF >> ${plotfile}
set terminal pdfcairo noenhanced size 29.7cm,21cm
set output "${escappedoutput}.pdf"
EOF
 fi
elif [ ${png} -eq 1 ]; then
 if [ "x$plottingtool" == "xpyxplot" ]; then
  cat << EOF >> ${plotfile}
set terminal png dpi ${dpi}
set output "${escappedoutput}.png"
EOF
 else
  cat << EOF >> ${plotfile}
set terminal pngcairo noenhanced size 29.7cm,21cm
set output "${escappedoutput}.png"
EOF
 fi
else
 echo "error: neither pdf or png output given"
 exit 1
fi

ncols=${#ycol[@]}

# if no ycol argument is given, sweep all the columns
if [ $ncols -eq 0 ]; then
 ncols=`grep -v \# $data | head -n1 | awk '{print NF}'`
 last=`expr ${ncols} - 2`
 for i in `seq 0 ${last}`; do
  ycol[$i]=`expr $i + 2`
 done
fi

# if not explicitly given, choose a suitable point interval
if [[ -z "${with[0]}" ]] && [[ -z "${pi[0]}" ]] ; then
#  x0=`grep -v \# $data | head -n1 | awk -v col=$xcol '{print $col}'`
#  xn=`grep -v \# $data | tail -n1 | awk -v col=$xcol '{print $col}'`
 n=`grep -v \# $data | wc -l`
 if [ $n -gt 10 ]; then
  last=`expr ${ncols} - 1`
  for i in `seq 0 ${last}`; do
   pi[$i]=`echo "scale=3; (0.5+${RANDOM}.0/32726.0)*($n/10.0)" | bc`
  done
 fi
fi

# if no titles given, check if the file contains a header with the column title
if [[ -z "${ti[0]}" ]] && [[ "`head -n1 $data | cut -c1`" == "#" ]]; then
 last=`expr ${ncols} - 1`
 for i in `seq 0 ${last}`; do
  if [ "x$plottingtool" == "xpyxplot" ]; then
   ti[$i]=`head -n1 $data | cut -c1 --complement | awk -v col=${ycol[$i]} '{print $col}' | sed s/_/\\\\\\\\_/g`
   # if the title is a single letter, we mark it as LaTeX math
   if [ `echo -n ${ti[$i]} | wc -m` -eq 1 ]; then
     ti[$i]="\$${ti[$i]}\$"
   fi
  else
   ti[$i]=`head -n1 $data | cut -c1 --complement | awk -v col=${ycol[$i]} '{print $col}'`
  fi
 done
 if [ -z "${xlabel}" ]; then
  if [ "x$plottingtool" == "xpyxplot" ]; then
   xlabel=`head -n1 $data | cut -c1 --complement | awk -v col=${xcol} '{print $col}' | sed s/_/\\\\\\\\_/g`
   # if the xlabel is a single letter, we mark it as LaTeX math
   if [ `echo -n ${xlabel} | wc -m` -eq 1 ]; then
     xlabel="\$${xlabel}\$"
   fi
  else
   xlabel=`head -n1 $data | cut -c1 --complement | awk -v col=${xcol} '{print $col}'`
  fi
  echo "set xlabel \"${xlabel}\"" >> ${plotfile}
 fi
fi

# if only one column was given, then put the title as the ylabel instead
if [[ $i -eq 1 ]] && [[ -z "${ylabel}" ]]; then
 ylabel=${ti[0]}
 if [ "x$plottingtool" == "xpyxplot" ]; then
  # if the xlabel is a single letter, we mark it as LaTeX math
  if [ `echo -n ${ylabel} | wc -m` -eq 1 ]; then
    ylabel="\$${ylabel}\$"
  fi
 fi
 echo "set ylabel \"${ylabel}\"" >> ${plotfile}
 ti[0]=""
fi

echo "plot \\" >> ${plotfile}

i=0
while [ ! -z "${ycol[$i]}" ]; do

# extension to pyxplot: it doesn't handle w lp pi 10
 if [ -z "${pi[$i]}" ]; then
  echo -n "\"$data\" u $xcol:${ycol[$i]} "  >> ${plotfile}

  if [ -z "${with[$i]}" ]; then
   echo -n " w lp" >> ${plotfile};
  else
   echo -n " w ${with[$i]}" >> ${plotfile};
  fi

  if [ ! -z "${lw[$i]}" ];    then  echo -n " lw ${lw[$i]}"        >> ${plotfile}; fi
  if [ ! -z "${lt[$i]}" ];    then  echo -n " lt ${lt[$i]}"        >> ${plotfile}; fi

  if [ ! -z "${pt[$i]}" ]; then
    echo -n " pt ${pt[$i]}"               >> ${plotfile}
  elif [ ! -z "${defaultpt[$i]}" ]; then
    echo -n " pt ${defaultpt[$i]}"        >> ${plotfile}
  fi

  if [ ! -z "${plw[$i]}" ];   then  echo -n " plw ${plw[$i]}"      >> ${plotfile}; fi
  if [ ! -z "${ps[$i]}" ];    then  echo -n " ps ${ps[$i]}"        >> ${plotfile}; fi

  if [ "x$plottingtool" == "xpyxplot" ]; then
    if [ ! -z "${color[$i]}" ]; then
      echo -n " color ${color[$i]}"         >> ${plotfile}
    elif [ ! -z "${pyxplot_defaultcolor[$i]}" ]; then
      echo -n " color ${pyxplot_defaultcolor[$i]}"  >> ${plotfile}
    fi
  else
    if [ ! -z "${color[$i]}" ]; then
      echo -n " lc rgb \"${color[$i]}\""         >> ${plotfile}
    elif [ ! -z "${gnuplot_defaultcolor[$i]}" ]; then
      echo -n " lc rgb \"${gnuplot_defaultcolor[$i]}\""  >> ${plotfile}
    fi
  fi

  if [ ! -z "${ti[$i]}" ];           then
   echo -n " ti \"${ti[$i]}\""             >> ${plotfile}
  else
   echo -n " ti \"\""                      >> ${plotfile}
  fi

 else
  echo -n "\"$data\" u $xcol:${ycol[$i]} every ${pi[$i]} w p"  >> ${plotfile}
  if [ ! -z "${lt[$i]}" ];    then  echo -n " lt ${lt[$i]}"        >> ${plotfile}; fi

  if [ ! -z "${pt[$i]}" ]; then
    echo -n " pt ${pt[$i]}"               >> ${plotfile}
  elif [ ! -z "${defaultpt[$i]}" ]; then
    echo -n " pt ${defaultpt[$i]}"        >> ${plotfile}
  fi

  if [ ! -z "${plw[$i]}" ];   then  echo -n " plw ${plw[$i]}"      >> ${plotfile}; fi
  if [ ! -z "${ps[$i]}" ];    then  echo -n " ps ${ps[$i]}"        >> ${plotfile}; fi
  if [ "x$plottingtool" == "xpyxplot" ]; then
    if [ ! -z "${color[$i]}" ]; then
      echo -n " color ${color[$i]}"         >> ${plotfile}
    elif [ ! -z "${pyxplot_defaultcolor[$i]}" ]; then
      echo -n " color ${pyxplot_defaultcolor[$i]}"  >> ${plotfile}
    fi
  else
    if [ ! -z "${color[$i]}" ]; then
      echo -n " lc rgb \"${color[$i]}\""         >> ${plotfile}
    elif [ ! -z "${gnuplot_defaultcolor[$i]}" ]; then
      echo -n " lc rgb \"${gnuplot_defaultcolor[$i]}\""  >> ${plotfile}
    fi
  fi

  if [ ! -z "${ti[$i]}" ];    then
   echo -n " ti \"${ti[$i]}\""    >> ${plotfile}
  else
   echo -n " ti \"\""             >> ${plotfile}
  fi

  echo -n ", \"$data\" u $xcol:${ycol[$i]} w l "  >> ${plotfile}
  if [ ! -z "${lw[$i]}" ];    then  echo -n " lw ${lw[$i]}"        >> ${plotfile}; fi
  if [ ! -z "${lt[$i]}" ];    then  echo -n " lt ${lt[$i]}"        >> ${plotfile}; fi
  if [ "x$plottingtool" == "xpyxplot" ]; then
    if [ ! -z "${color[$i]}" ]; then
      echo -n " color ${color[$i]}"         >> ${plotfile}
    elif [ ! -z "${pyxplot_defaultcolor[$i]}" ]; then
      echo -n " color ${pyxplot_defaultcolor[$i]}"  >> ${plotfile}
    fi
  else
    if [ ! -z "${color[$i]}" ]; then
      echo -n " lc rgb \"${color[$i]}\""         >> ${plotfile}
    elif [ ! -z "${gnuplot_defaultcolor[$i]}" ]; then
      echo -n " lc rgb \"${gnuplot_defaultcolor[$i]}\""  >> ${plotfile}
    fi
  fi
  echo -n " ti \"\""             >> ${plotfile}
 fi


 i=`echo $i + 1 | bc`
 if [ ! -z "${ycol[$i]}" ]; then
  echo ",\\" >> ${plotfile}
 fi
done

echo >> ${plotfile}

if [[ ${pdf} -ne 1 ]] && [[ ${png} -eq -1 ]]; then
 if [ "x$plottingtool" == "xpyxplot" ]; then
  cat << EOF >> ${plotfile}
set terminal png dpi ${dpi}
set output "${escappedoutput}.png"
replot
EOF
 else
  cat << EOF >> ${plotfile}
set terminal pngcairo size 29.7cm,21cm
set output "${escappedoutput}.png"
replot
EOF
 fi
fi

# call pyxplot/gnuplot
if [ "x$plottingtool" == "xpyxplot" ]; then
  pyxplot ${plotfile}
else
  gnuplot ${plotfile}
fi  

if [ ! -z "$debug" ]; then
 echo "debug: plot file saved as ${plotfile}"
else
 rm ${plotfile}
fi

if [ "x$plottingtool" == "xpyxplot" ]; then
# if in interactive mode, show the resulting pdf
# # explanation: http://stackoverflow.com/questions/4261876/check-if-bash-script-was-invoked-from-a-shell-or-another-script-application
  if [[ ! ${pdf} -eq 0 ]] && [[ "`ps -o stat= -p $PPID | grep s`" == "Ss" ]]; then
    xdg-open "${escappedoutput}.pdf"
  fi
fi
